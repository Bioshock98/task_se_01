package ru.pyshinskiy.tm.util;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public class ProjectUtil {

    public static void printProjects(List<Project> projects, boolean selectTasks) {
        for (int i = 0; i < projects.size(); i++) {
            final Project project = projects.get(i);
            System.out.println((i + 1) + "." + " " + project.getName());
            if (selectTasks) {
                List<Task> tasks = project.getTasks();
                for (int j = 0; j < tasks.size(); j++) {
                    Task task = tasks.get(j);
                    System.out.println("  " + (j + 1) + "." + " " + task.getName());
                }
            }
        }
    }
}
